package com.student.click.util;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.student.click.studentmanagementsystem.R;

public class PagerAdapter extends FragmentPagerAdapter {


   private  String[] tabMenu;//={"View Student","Add Student"};
    private int pageCount;//= 2;
    private Context context;
    private Fragment[] fragments;

    public PagerAdapter(FragmentManager fm, Context context, Fragment[] fragments) {
        super(fm);
        this.context = context;
        tabMenu = context.getResources().getStringArray(R.array.tab_menu);
        pageCount = tabMenu.length;
        this.fragments = fragments;
    }


    @Override
    public Fragment getItem(int position) {
        return fragments[position];
//        switch(position){
//            case 0:
//                return new ViewStudentsFragment();
//            case 1:
//                return new AddStudentFragment();
//        }
//        return null;
    }

    @Override
    public int getCount() {
        return fragments.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabMenu[position];
    }
}
