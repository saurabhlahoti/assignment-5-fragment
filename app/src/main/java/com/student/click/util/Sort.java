package com.student.click.util;


import com.student.click.entities.Student;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Sort {

    public ArrayList<Student> sortByName(ArrayList<Student> list) { // function to sort by name
        Collections.sort(list, byName());
       return list;
    }
    public ArrayList<Student> sortByRoll(ArrayList<Student> list){
        Collections.sort(list,byRoll());
        return list;
    }
    private Comparator<Student> byName() { // making a customized
        // comparator
        return new Comparator<Student>() {
            // @Override
            public int compare(Student c1, Student c2) {
                return c1.getName().toLowerCase().compareTo(c2.getName().toLowerCase());
            }
        };
    }
    private Comparator<Student> byRoll() { // making a customized
        // comparator
        return new Comparator<Student>() {
            // @Override
            public int compare(Student c1, Student c2) {
                return ((Integer.parseInt(c1.getRoll()))-(Integer.parseInt(c2.getRoll())));
            }
        };
    }

}

//
//    static void sortByName() { // function to sort by name
//        ArrayList<Student> customList = new ArrayList<Student>(
//                custHash.values());
//        Collections.sort(customList, byName());
//        for (int i = 0; i < customList.size(); i++) {
//            System.out.println(customList.get(i));
//        }
//    }