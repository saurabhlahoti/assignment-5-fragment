package com.student.click.util;

import android.os.Environment;

import java.io.File;

public class DeleteData {
    public DeleteData() {
    }
    public void deleteStudent(String imgName) {
        File image = new File(Environment.getExternalStorageDirectory().toString() + File.separator + "StudentMS" + File.separator + "SAVED" + File.separator + imgName + ".jpg");
        if(image.exists()) {
            image.delete();
        }
    }
}
