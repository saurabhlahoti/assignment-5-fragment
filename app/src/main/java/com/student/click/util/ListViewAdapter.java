package com.student.click.util;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.student.click.entities.Student;
import com.student.click.studentmanagementsystem.R;

import java.util.ArrayList;

public class ListViewAdapter extends BaseAdapter {

    ArrayList<Student> studentList;
    LayoutInflater inflater;
    Context context;


    public ListViewAdapter(Context context, ArrayList<Student> list) {
        this.studentList = list;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return studentList.size();
    }

    @Override
    public Student getItem(int position) {
        return studentList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder mViewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.student_list_structure, parent, false);
            mViewHolder = new MyViewHolder(convertView);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        Student student = getItem(position);
        mViewHolder.name.setText(student.getName());
        mViewHolder.roll.setText(student.getRoll());
        return convertView;
    }

//        Student student = getItem(position);
//        if (student != null) {
//            TextView name = (TextView) view.findViewById(R.id.studentName);
//            TextView roll = (TextView) view.findViewById(R.id.studentRoll);
//            if (name != null) {
//                name.setText(student.getName());
//            }
//            if (roll != null) {
//                roll.setText(student.getRoll());
//            }
//        }
//        return view;
    private class MyViewHolder {
        TextView name, roll;
        public MyViewHolder(View item) {
            name = (TextView) item.findViewById(R.id.studentNameList);
            roll = (TextView) item.findViewById(R.id.studentRollList);
        }
    }

    public ArrayList<Student> getStudentList() {
        return studentList;
    }


    public void setStudentList(ArrayList<Student> studentList) {
        this.studentList = studentList;
    }

}

