package com.student.click.interfaces;


import android.os.Bundle;

public interface FragmentCommunicationListener {
    public void passMsg( Bundle msg);
}
