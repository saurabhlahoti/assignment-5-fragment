package com.student.click.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.student.click.fragments.AddStudentFragment;
import com.student.click.fragments.ViewStudentsFragment;
import com.student.click.interfaces.FragmentCommunicationListener;
import com.student.click.studentmanagementsystem.R;
import com.student.click.util.PagerAdapter;

public class MainActivity extends FragmentActivity implements FragmentCommunicationListener {

    ViewPager viewPager;
    PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //createTabs();
        viewPager = (ViewPager) findViewById(R.id.pager);
        Fragment[] fragments = new Fragment[]{
                new ViewStudentsFragment(),
                new AddStudentFragment()
        };
        pagerAdapter = new PagerAdapter(getSupportFragmentManager(), this, fragments);
        viewPager.setAdapter(pagerAdapter);
        PagerTabStrip mPagerTabStrip = (PagerTabStrip) findViewById(R.id.pagerStrip);
        for (int i = 0; i < mPagerTabStrip.getChildCount(); ++i) {
            View nextChild = mPagerTabStrip.getChildAt(i);
            if (nextChild instanceof TextView) {
                TextView textViewToConvert = (TextView) nextChild;
                textViewToConvert.setTextColor(0xFFB7FFC9);
                textViewToConvert.setTextSize(23);
            }

        }
    }

    @Override
    public void passMsg( Bundle msg) {

        int currentItem = viewPager.getCurrentItem();
        Fragment targetFragment;
        if (currentItem == 0) {
            targetFragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + 1);
            ((AddStudentFragment) targetFragment).renderData(msg);
            viewPager.setCurrentItem(1);
        } else {
            targetFragment = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + 0);
            ((ViewStudentsFragment) targetFragment).updateList(msg,viewPager);
            viewPager.setCurrentItem(0);
        }
    }

    @Override
    public void onBackPressed() {
       ViewStudentsFragment fragment = (ViewStudentsFragment) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + 0);
        if (fragment.getChildFragmentManager().getBackStackEntryCount() != 0) {
            fragment.getChildFragmentManager().popBackStack();
        }
        else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



}