package com.student.click.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.student.click.entities.Student;
import com.student.click.interfaces.FragmentCommunicationListener;
import com.student.click.studentmanagementsystem.R;
import com.student.click.util.Constant;
import com.student.click.util.ImageDisplay;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;


public class AddStudentFragment extends Fragment {
    int id;
    ImageView profileImage;
    String name, oldRoll = "", roll, branch, image;
    ImageButton setImageToDefaultButton;
    ImageDisplay imageDisplay;
    Boolean flag = false;
    ArrayList<String> imageList = new ArrayList<String>();
    Button addStudentButton;
    FragmentCommunicationListener fragComm = null;
    View view;

    private static AddStudentFragment addStudentFragment;

    public void renderData(Bundle msg) {
        Student studentEdit = (Student) msg.getSerializable("editField");
        id = msg.getInt("id", -1);
        if (id != -1) {
            flag = true;
            EditText name = (EditText) view.findViewById(R.id.name);
            name.setText(studentEdit.getName());
            EditText branch = (EditText) view.findViewById(R.id.branch);
            branch.setText(studentEdit.getBranch());
            EditText roll = (EditText) view.findViewById(R.id.roll);
            roll.setText(studentEdit.getRoll());
            oldRoll = studentEdit.getRoll();
            image = studentEdit.getImage();
            String imageName = image + ".jpg";
            if (imageName.compareTo("profile.jpg") == 0) {
                profileImage.setImageResource(imageDisplay.profileImage());
            } else {
                profileImage.setImageBitmap(imageDisplay.renderImage(imageName, getActivity()));
                setImageToDefaultButton.setVisibility(View.VISIBLE);
            }
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        image = "profile";  //initializing
        view = inflater.inflate(R.layout.fragment_add_students, container, false);
        imageDisplay = new ImageDisplay();
        addStudentButton = (Button) view.findViewById(R.id.addStudentButton);
        setImageToDefaultButton = (ImageButton) view.findViewById(R.id.setDefaultImageButton);
        setImageToDefaultButton.setVisibility(View.GONE);
        profileImage = (ImageView) view.findViewById(R.id.profileImage);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        setImageToDefaultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                profileImage.setImageResource(imageDisplay.profileImage());
                setImageToDefaultButton.setVisibility(View.GONE);
            }
        });

        addStudentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv;
                tv = (TextView) view.findViewById(R.id.name);
                name = tv.getText().toString();
                tv = (TextView) view.findViewById(R.id.roll);
                roll = tv.getText().toString();
                tv = (TextView) view.findViewById(R.id.branch);
                branch = tv.getText().toString();
                if (validate(view, name, roll, branch)) {
                    Bundle bundle = new Bundle();
                    if (flag) {
                        bundle.putInt("id", id);
                    }
                    bundle.putSerializable("addField", new Student(name, roll, branch, image));
                    fragComm.passMsg(bundle);
                }
                image = "profile";
                ((TextView) view.findViewById(R.id.name)).setText("");
                ((TextView) view.findViewById(R.id.roll)).setText("");
                ((TextView) view.findViewById(R.id.branch)).setText("");
                ((ImageView)view.findViewById(R.id.profileImage)).setImageResource(R.mipmap.profile);
                setImageToDefaultButton.setVisibility(View.GONE);
                id=-1;
                flag=false;
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof FragmentCommunicationListener) {
            fragComm = (FragmentCommunicationListener) activity;
        } else {
            throw new ClassCastException();
        }
    }


    void selectImage() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Add Profile Image");
        dialog.setItems(R.array.image_menu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == Constant.CAMERA_OPTION) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                    startActivityForResult(intent, Constant.TOKEN_IMAGE_FROM_CAMERA);
                } else if (item == Constant.GALLERY_OPTION) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(intent, Constant.TOKEN_IMAGE_FROM_GALLERY);
                } else {
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }


    Boolean validate(View view, String name, String roll, String branch) {
        EditText et;
        if (name.compareTo("") == 0) {
            et = (EditText) view.findViewById(R.id.name);
            et.setHovered(true);
            et.setError("Field Empty");
            return false;
        }
        if (branch.compareTo("") == 0) {
            et = (EditText) view.findViewById(R.id.branch);
            et.setHovered(true);
            et.setError("Field Empty");
            return false;
        }
        if (roll.compareTo("") == 0) {
            et = (EditText) view.findViewById(R.id.roll);
            et.setHovered(true);
            et.setError("Field Empty");
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == getActivity().RESULT_OK) {
            setImageToDefaultButton.setVisibility(View.VISIBLE);
            if (requestCode == Constant.TOKEN_IMAGE_FROM_CAMERA) {
                File tempCamFile = new File(Environment.getExternalStorageDirectory() + "/temp.jpg");
                try {
                    Bitmap bitmap;//=MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.fromFile(file));
                    BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(tempCamFile.getAbsolutePath(), bitmapOptions);
                    profileImage.setImageBitmap(bitmap);

                    File myDir = new File(Constant.IMAGE_DIRECTORY);
                    myDir.mkdirs();
                    OutputStream outFile = null;
                    image = String.valueOf(System.currentTimeMillis());
                    File finalFile = new File(myDir.getAbsolutePath(), image + ".jpg");
                    tempCamFile.delete();
                    try {
                        outFile = new FileOutputStream(finalFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, outFile);
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == Constant.TOKEN_IMAGE_FROM_GALLERY) {
                Uri selectedImage = data.getData();
                try {

                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                    profileImage.setImageBitmap(bitmap);
                    File myDir = new File(Constant.IMAGE_DIRECTORY);
                    try {
                        myDir.mkdirs();
                    } catch (Exception e) {
                        Log.e("DEBUG", "Could not write file " + e.getMessage());
                    }
                    image = String.valueOf(System.currentTimeMillis());
                    File finalFile = new File(myDir.getAbsolutePath(), image + ".jpg");
                    try {
                        FileOutputStream out = new FileOutputStream(finalFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 75, out);
                        out.flush();
                        out.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            imageList.add(image);
        }
    }
}
