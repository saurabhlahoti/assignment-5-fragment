package com.student.click.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.student.click.entities.Student;
import com.student.click.studentmanagementsystem.R;
import com.student.click.util.ImageDisplay;

public class Details extends Fragment {
    View view,parentView;
    static Details details;
    Context context;

    static Details getInstance() {
        if (details == null) {
            details = new Details();
        }
        return details;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context=container.getContext();
        Bundle bundle = getArguments();
        view=inflater.inflate(R.layout.display_details, container, false);
        Student student = (Student) bundle.getSerializable("viewField");
        ((TextView) view.findViewById(R.id.nameDetail)).setText(student.getName());
        ((TextView)view.findViewById(R.id.rollDetail)).setText(student.getRoll());
        ((TextView)view.findViewById(R.id.branchDetail)).setText(student.getBranch());
        view.findViewById(R.id.displayDetails).setVisibility(View.VISIBLE);
        String imgName= student.getImage();
        ImageView imgView = (ImageView)view.findViewById(R.id.imageDetail);
        ImageDisplay imageDisplay = new ImageDisplay();
        if(imgName.compareTo("profile")==0){
            imgView.setImageResource(imageDisplay.profileImage());
        }else{
            imgView.setImageBitmap(imageDisplay.renderImage(imgName,context));
        }
        LinearLayout layout = (LinearLayout)view.findViewById(R.id.displayDetails);
        layout.setClickable(true);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getParentFragment().getChildFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.remove(Details.this);
                ft.commit();
                if (fm.getBackStackEntryCount() != 0) {
                    fm.popBackStack();
                }
            }
        });
        return view;
    }
    @Override
    public void onDetach() {
        super.onDetach();
        view.findViewById(R.id.displayDetails).setVisibility(View.GONE);
        ViewStudentsFragment parent = (ViewStudentsFragment)getParentFragment();
        parent.setLayout(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        view.findViewById(R.id.displayDetails).setVisibility(View.GONE);
    }
    @Override
    public void setArguments(Bundle bundle) {
        super.setArguments(bundle);
    }
}
