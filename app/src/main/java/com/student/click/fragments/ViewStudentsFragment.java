package com.student.click.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.student.click.entities.Student;
import com.student.click.interfaces.FragmentCommunicationListener;
import com.student.click.studentmanagementsystem.R;
import com.student.click.util.Constant;
import com.student.click.util.DeleteData;
import com.student.click.util.GridViewAdapter;
import com.student.click.util.ListViewAdapter;
import com.student.click.util.Sort;

import java.util.ArrayList;


public class ViewStudentsFragment extends Fragment {

    ArrayList<Student> studentList = new ArrayList<>();
    ListView list;
    ListViewAdapter listAdapter;
    GridView grid;
    FragmentCommunicationListener fragComm = null;
    int resIdView, resIdSort;
    GridViewAdapter gridAdapter;
    ImageButton toggleView, toggleSort;
    View view;
    ViewPager viewPager = null;
    FragmentTransaction transaction;
    Fragment details;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_view_students, container, false);
        toggleView = (ImageButton) view.findViewById(R.id.viewImageButton);
        toggleSort = (ImageButton) view.findViewById(R.id.sortImageButton);
        view.findViewById(R.id.container).setVisibility(View.GONE);
        resIdView = R.mipmap.grid;
        resIdSort = R.mipmap.sortname;
        toggleView.setImageResource(resIdView);
        toggleSort.setImageResource(resIdSort);
        list = (ListView) view.findViewById(R.id.studentList);
        grid = (GridView) view.findViewById(R.id.studentGrid);
        listAdapter = new ListViewAdapter(getActivity(), studentList);
        gridAdapter = new GridViewAdapter(getActivity(), studentList);
        list.setAdapter(listAdapter);
        grid.setAdapter(gridAdapter);
        grid.setVisibility(View.GONE);

        toggleView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (resIdView == R.mipmap.grid) {
                    resIdView = R.mipmap.list;
                    list.setVisibility(View.GONE);
                    grid.setVisibility(View.VISIBLE);

                } else {
                    resIdView = R.mipmap.grid;
                    grid.setVisibility(View.GONE);
                    list.setVisibility(View.VISIBLE);
                }
                toggleView.setImageResource(resIdView);
            }

        });

        toggleSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Sort sortClass = new Sort();
                if (resIdSort == R.mipmap.sortname) {
                    resIdSort = R.mipmap.sortroll;
                    studentList = sortClass.sortByName(studentList);
                } else {
                    resIdSort = R.mipmap.sortname;
                    studentList = sortClass.sortByRoll(studentList);
                }
                try {
                    toggleSort.setImageResource(resIdSort);
                } catch (NullPointerException e) {
                    e.fillInStackTrace();
                }
                listAdapter.notifyDataSetChanged();
                gridAdapter.notifyDataSetChanged();
            }
        });

        registerForContextMenu(list);
        registerForContextMenu(grid);
        return view;
    }

    public void updateList(Bundle bundle, ViewPager vp) {
        viewPager = vp;
        int id = bundle.getInt("id", -1);
        if (id != -1) {
            studentList.remove(id);
        }
        Student studentNew = (Student) bundle.getSerializable("addField");
        if (studentNew != null) {
            studentList.add(studentNew);
            if (listAdapter == null || gridAdapter == null) {
                list = (ListView) view.findViewById(R.id.studentList);
                grid = (GridView) view.findViewById(R.id.studentGrid);
                listAdapter = new ListViewAdapter(getActivity(), studentList);
                gridAdapter = new GridViewAdapter(getActivity(), studentList);
                list.setAdapter(listAdapter);
                grid.setAdapter(gridAdapter);
            }

        }
        listAdapter.notifyDataSetChanged();
        gridAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof FragmentCommunicationListener) {
            fragComm = (FragmentCommunicationListener) activity;
        } else {
            throw new ClassCastException();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo) {

        if (view.getId() == R.id.studentList || view.getId() == R.id.studentGrid) {
            //AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
            String[] menuItems = getResources().getStringArray(R.array.menu);
            for (int i = 0; i < menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    public void setLayout(Boolean bool) {
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.layoutButton);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            child.setEnabled(bool);
        }
        layout = (LinearLayout) view.findViewById(R.id.layoutListGrid);
        for (int i = 0; i < layout.getChildCount(); i++) {
            View child = layout.getChildAt(i);
            child.setEnabled(bool);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        studentList = listAdapter.getStudentList();
        Student student = studentList.get(info.position);
        Bundle bundle = new Bundle();
        switch (menuItemIndex) {
            case Constant.VIEW_STUDENT_DETAIL:
                bundle.putSerializable("viewField", student);
                view.findViewById(R.id.container).setVisibility(View.VISIBLE);
                setLayout(false);
                transaction = getChildFragmentManager().beginTransaction();
                transaction.addToBackStack(null);
                details = new Details();
                transaction.replace(R.id.container, details, "task");
                transaction.commit();
                details.setArguments(bundle);
                break;
            case Constant.EDIT_STUDENT_DETAIL:
                bundle.putSerializable("editField", student);
                bundle.putInt("id", info.position);
                fragComm.passMsg(bundle);
                break;
            case Constant.DELETE_STUDENT_DETAIL:
                new DeleteData().deleteStudent(studentList.get(info.position).getImage());
                studentList.remove(info.position);
                Toast.makeText(getActivity(), "Deleted", Toast.LENGTH_SHORT).show();
            default:
        }
        listAdapter.setStudentList(studentList);
        gridAdapter.setStudentList(studentList);
        listAdapter.notifyDataSetChanged();
        gridAdapter.notifyDataSetChanged();
        return true;
    }
}
